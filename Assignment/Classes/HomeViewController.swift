//
//  HomeViewController.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UISearchBarDelegate,UIPopoverPresentationControllerDelegate, delegatePopUpClass{

    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var viewNavbar: UIView! {
        didSet {
            setShadowEffectForTopView(bgView: viewNavbar, cornerRadius: 0)
        }
    }
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = setRandomFont(withSize: 16.0, withFontName: FONT_RAJDHANI_SEMIBOLD)
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblViewHome: UITableView!
    
    var filterdata:[structGetRepositories] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.gray.cgColor
        
        if let textFields = searchBar.subviews.first?.subviews.compactMap({ $0 as? UITextField }) {
            textFields.forEach { $0.tintColor = UIColor.darkGray }
        }
        
        tblViewHome.tableFooterView = UIView (frame: CGRect(x:0, y:0, width:0, height:0))
        tblViewHome.estimatedRowHeight = 120.0
        tblViewHome.rowHeight = UITableView.automaticDimension
        
        tblViewHome.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        
        searchBar.delegate = self
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // filterdata  = searchText.isEmpty ? data : data.filter {(item : String) -> Bool in
        //        filterdata = searchText.isEmpty ? data : data.filter { $0.contains(searchText) }
        //return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        
        self.ServerCallSearchRepositories(searchText:searchText )

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
    }
    
    @IBAction func btnFilterClicked(_ sender: Any) {
        let MenuPopupVC = self.storyboard?.instantiateViewController(withIdentifier:"FilterPopoverViewController") as? FilterPopoverViewController
        MenuPopupVC?.objDelegatePopUpClass = self
        
        MenuPopupVC?.modalPresentationStyle =  UIModalPresentationStyle.popover
        let popover = MenuPopupVC?.popoverPresentationController
        popover?.permittedArrowDirections = UIPopoverArrowDirection .up  //UIPopoverArrowDirection(rawValue: 0)
        MenuPopupVC?.preferredContentSize = CGSize(width:150,height:180)
        popover?.delegate = self
        popover?.sourceView = btnFilter
        popover?.sourceRect = CGRect(x: 0, y: 6, width: btnFilter.frame.size.width, height: btnFilter.frame.size.height) //btnMore.bounds
        self.present(MenuPopupVC!, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return .none
    }
   
    
    func delegatecellClickedInPopUp(intValue: Int) {
        dismiss(animated: false, completion: nil)
        
        if intValue == 0 {
            self.filterdata = self.filterdata.sorted(by: { $0.strWatchers_count > $1.strWatchers_count })
        }
        else if intValue == 1 {
             self.filterdata = self.filterdata.sorted(by: { $0.strWatchers_count < $1.strWatchers_count })
        }
        if intValue == 2 {
            self.filterdata = self.filterdata.sorted(by: { $0.intSize > $1.intSize })
        }
        else if intValue == 3 {
            self.filterdata = self.filterdata.sorted(by: { $0.intSize < $1.intSize })
        }
        
        self.tblViewHome.reloadData()

    }
    
    // Get Repositories
    func ServerCallSearchRepositories(searchText: String) {
        
        if !checkNet() {
            
            self.popupAlert(title: "No Internet", message: " Oops, Please check your internet connection!", actionTitles: ["Ok"], actions:[{action1 in
                
                },{action2 in
                    
                }, nil])
            
            return
        }
        
        APIs.sharedInstance.Service_Call(withParameters: searchText, withMethodName: .getRepositories, complitionHandler: {(_ response: Any, _ error: Error?, _ isSuccess: Bool) -> Void in
            
            DispatchQueue.main.async(execute: {() -> Void in
                if isSuccess {
                    
                    let result : NSDictionary = nullConditionForDictionary(dict: response) as NSDictionary
                    let arrData = JsonParser.sharedInstance.getRepositoriesDetails(resultObj: result) as! [structGetRepositories]
                    
                    self.filterdata = arrData.sorted(by: { $0.strWatchers_count > $1.strWatchers_count })

                    self.tblViewHome.reloadData()
                }
                else {
                    print(response as! String)
                }
            })
            
        })
    }
    
    
}


// MARK: - Table View DataSource
extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdata.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.selectionStyle = .none
        
        cell.structRepo = filterdata[indexPath.row]
        
        return cell
    }
    
}

// MARK: - Table View Delegate
extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "RepoDetailsViewController") as! RepoDetailsViewController
        VC.selectedRepoObj = filterdata[indexPath.row]
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
}


extension UIViewController {
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
}
