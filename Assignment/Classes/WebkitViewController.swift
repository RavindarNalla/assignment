//
//  WebkitViewController.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit
import WebKit

class WebkitViewController: UIViewController,WKNavigationDelegate {
    
    
    @IBOutlet weak var viewNavbar: UIView! {
        didSet {
            setShadowEffectForTopView(bgView: viewNavbar, cornerRadius: 0)
        }
    }
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = setRandomFont(withSize: 16.0, withFontName: FONT_RAJDHANI_BOLD)
        }
    }
    @IBOutlet weak var viewWebBg: UIView!
    var strUrl : String!
    var webView : WKWebView!
    var loader : SampleView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let url = URL(string: strUrl)
        
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-56))
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url!))
        self.viewWebBg.addSubview(webView)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    //MARK:- WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        loader = showLoader(message: "Loading...")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        hideLoader(loader: loader )
    }

}
