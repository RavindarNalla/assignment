//
//  RepoDetailsViewController.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class RepoDetailsViewController: UIViewController {
    @IBOutlet weak var viewNavbar: UIView! {
        didSet {
            setShadowEffectForTopView(bgView: viewNavbar, cornerRadius: 0)
        }
    }
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = setRandomFont(withSize: 16.0, withFontName: FONT_RAJDHANI_BOLD)
        }
    }
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_REGULAR)
        }
    }
    @IBOutlet weak var lblProjectLink: UILabel! {
        didSet {
            lblProjectLink.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_SEMIBOLD)
        }
    }
    @IBOutlet weak var btnClickhere: UIButton! {
        didSet {
            btnClickhere.titleLabel!.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_SEMIBOLD)
        }
    }
    @IBOutlet weak var lblDescription: UILabel! {
        didSet {
            lblDescription.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_SEMIBOLD)
        }
    }
    @IBOutlet weak var txtView: UITextView! {
        didSet {
            txtView.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_REGULAR)
            txtView.layer.cornerRadius = 2.0
            txtView.layer.borderWidth = 1.0
            txtView.layer.borderColor = UIColor.gray.cgColor
        }
    }
    @IBOutlet weak var lblContributors: UILabel! {
        didSet {
            lblContributors.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_SEMIBOLD)
        }
    }
    @IBOutlet weak var collectionViewContributors: UICollectionView! 
    
    var selectedRepoObj: structGetRepositories?
    var contributorsData:[structGetContributors] = []
    
    var isFromContributorDetails:Bool = false
    
    var VCContributor : ContributorDetailsViewController!
    


    override func viewDidLoad() {
        super.viewDidLoad()

        self.updateRepodetails(selectedRepoObject: selectedRepoObj!)
        // Do any additional setup after loading the view.
        
    collectionViewContributors.register(UINib(nibName: "ContributorsCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ContributorsCollectionViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    
    func updateRepodetails(selectedRepoObject: structGetRepositories) {
        imgView.dowloadFromServer(link: selectedRepoObject.strAvatar_url, contentMode: .scaleAspectFit)
        
        lblName.attributedText = setAttrStrForLabel(str1: "Name : ", str2: "\(String(describing: selectedRepoObject.strName))")
        txtView.text = "\(String(describing: selectedRepoObject.strDescription))"
        ServerCallContributors(contributorUrl: selectedRepoObject.strContributors_url)
    }
    
    
    
    
    // Get Contributors
    func ServerCallContributors(contributorUrl: String) {
        
        if !checkNet() {
            
            self.popupAlert(title: "No Internet", message: " Oops, Please check your internet connection!", actionTitles: ["Ok"], actions:[{action1 in
                
                },{action2 in
                    
                }, nil])
            
            return
        }
        
        let loader = showLoader(message: "Loading...")

        APIs.sharedInstance.Service_Call(withParameters: contributorUrl, withMethodName: .getContributors, complitionHandler: {(_ response: Any, _ error: Error?, _ isSuccess: Bool) -> Void in
            
            DispatchQueue.main.async(execute: {() -> Void in
                if isSuccess {
        
                    let result : NSArray = nullConditionForArray(array: response) as NSArray
                    
                    self.contributorsData = JsonParser.sharedInstance.getContributorsDetails(resultObj: result) as! [structGetContributors]
                    
                    self.collectionViewContributors.reloadData()
                    
                    hideLoader(loader: loader)
                }
                else {
                    print(response as! String)
                    hideLoader(loader: loader)

                }
            })
            
        })
        
        hideLoader(loader: loader)

    }

    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClickhereClicked(_ sender: Any) {
        
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "WebkitViewController") as! WebkitViewController
        VC.strUrl = selectedRepoObj?.strProjectLink
        self.navigationController?.pushViewController(VC, animated: true)
    }
}


extension RepoDetailsViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.contributorsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContributorsCollectionViewCell", for: indexPath) as! ContributorsCollectionViewCell
        
        cell.structContributor = contributorsData[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (view.frame.size.width-30) / 5, height:80)
    }
    
}

extension RepoDetailsViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isFromContributorDetails {
            
            VCContributor.updateContributorsDetails(selectedContributorObject: contributorsData[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "ContributorDetailsViewController") as! ContributorDetailsViewController
            VC.selectedContributorObj = contributorsData[indexPath.row]
            //        VC.VCRepoDetails = self
            self.navigationController?.pushViewController(VC, animated: true)
            //        self.present(VC, animated: true) { }
        }
        

        
    }
    
}
