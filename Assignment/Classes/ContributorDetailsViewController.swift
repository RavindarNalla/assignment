//
//  ContributorDetailsViewController.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class ContributorDetailsViewController: UIViewController {

    
    @IBOutlet weak var viewNavbar: UIView! {
        didSet {
            setShadowEffectForTopView(bgView: viewNavbar, cornerRadius: 0)
        }
    }
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = setRandomFont(withSize: 16.0, withFontName: FONT_RAJDHANI_BOLD)
        }
    }
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var tblViewContributor: UITableView!
    
    var selectedContributorObj: structGetContributors?
    var filterdata:[structGetRepositories] = []
//    var VCRepoDetails : RepoDetailsViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        updateContributorsDetails(selectedContributorObject: selectedContributorObj!)
        
    }
    
    func updateContributorsDetails(selectedContributorObject: structGetContributors) {
        
        ServerCallRepoList(repoListUrl: selectedContributorObj!.strRepo_url)
        // Do any additional setup after loading the view.
        tblViewContributor.tableFooterView = UIView (frame: CGRect(x:0, y:0, width:0, height:0))
        tblViewContributor.estimatedRowHeight = 120.0
        tblViewContributor.rowHeight = UITableView.automaticDimension
        
        tblViewContributor.register(UINib(nibName: "RepoListTableViewCell", bundle: nil), forCellReuseIdentifier: "RepoListTableViewCell")
        
        imgView.dowloadFromServer(link: selectedContributorObject.strAvatar_url, contentMode: .scaleAspectFit)
        
        lblTitle.text = "\(String(describing: selectedContributorObject.strName))"
    }
    
    // Get Contributors
    func ServerCallRepoList(repoListUrl: String) {
        
        if !checkNet() {
            
            self.popupAlert(title: "No Internet", message: " Oops, Please check your internet connection!", actionTitles: ["Ok"], actions:[{action1 in
                
                },{action2 in
                    
                }, nil])
            
            return
        }
        let loader = showLoader(message: "Loading...")

        APIs.sharedInstance.Service_Call(withParameters: repoListUrl, withMethodName: .getRepolist, complitionHandler: {(_ response: Any, _ error: Error?, _ isSuccess: Bool) -> Void in
            
            DispatchQueue.main.async(execute: {() -> Void in
                if isSuccess {
                    
                    let result : NSArray = nullConditionForArray(array: response) as NSArray
                    
                    self.filterdata = JsonParser.sharedInstance.getRepoListDetails(resultObj: result) as! [structGetRepositories]
    
                    self.tblViewContributor.reloadData()
                    
                    hideLoader(loader: loader)
                }
                else {
                    print(response as! String)
                }
            })
            
        })
        
        hideLoader(loader: loader)

    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true) { }
    }

}


// MARK: - Table View DataSource
extension ContributorDetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterdata.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
 
            return 36.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     
            let view = UIView()
            view.backgroundColor = UIColor .white
            let label = UILabel()
            label.frame = CGRect(x: 16, y: 0, width: 250, height:30)
            label.font = setRandomFont(withSize: 14, withFontName: FONT_RAJDHANI_SEMIBOLD)
            label.textColor = UIColor.black
            label.text = "Repo list"
            view.addSubview(label)
            
            return view
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoListTableViewCell") as! RepoListTableViewCell
        cell.selectionStyle = .none
        
        cell.structRepo = self.filterdata[indexPath.row]
        
        return cell
    }
    
}

// MARK: - Table View Delegate
extension ContributorDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "RepoDetailsViewController") as! RepoDetailsViewController
        VC.selectedRepoObj = filterdata[indexPath.row]
        VC.isFromContributorDetails = true
        VC.VCContributor = self
        self.navigationController?.pushViewController(VC, animated: true)

//        VCRepoDetails.updateRepodetails(selectedRepoObject: filterdata[indexPath.row])
//        self.dismiss(animated: true) { }

        
    }
}
