//
//  FilterPopoverViewController.swift
//  Assignment
//
//  Created by Ravindar on 01/04/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit
protocol delegatePopUpClass: class
{
    func delegatecellClickedInPopUp(intValue : Int)
}

class FilterPopoverViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblMenuPopup: UITableView!
    
    var arrayOfMenuList:[String] = []
    weak var objDelegatePopUpClass:delegatePopUpClass?
    
    var strVCName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tblMenuPopup.tableFooterView = UIView (frame: CGRect(x:0, y:0, width:0, height:0))
        
        tblMenuPopup.estimatedRowHeight = 44.0
        tblMenuPopup.rowHeight = UITableView.automaticDimension
        
        arrayOfMenuList = ["Watchers Hight-Low","Watchers Low-Hight", "size Hight-Low", "size Low-Hight"]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:TableView Delegates
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayOfMenuList.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        //        cell.selectionStyle = .none
        
        cell.textLabel?.font = UIFont(name: FONT_RAJDHANI_REGULAR, size: 13)! //setRandomFont(withSize: 14.0, withFontName: FONTROBOTO_REGULAR)
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = arrayOfMenuList[indexPath.row]
        
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        objDelegatePopUpClass?.delegatecellClickedInPopUp(intValue: indexPath.row)
    }
    
}
