//
//  AppDefines.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import Foundation
import UIKit

let IPHONE_4_SCREEN_HEIGHT = 480
let IPHONE_4_SCREEN_WIDTH = 320

let IPHONE_5_SCREEN_HEIGHT = 568 //5S,SE
let IPHONE_6_SCREEN_HEIGHT = 667     //6S, 7
let IPHONE_6PLUS_SCREEN_HEIGHT  = 736 // 6s Plus, 7 Plus

let IPHONE_5_SCREEN_WIDTH  = 320 //5S,SE
let IPHONE_6_SCREEN_WIDTH = 375   //6S, 7
let IPHONE_6PLUS_SCREEN_WIDTH = 414 // 6s Plus, 7 Plus

let IPAD_SCREEN_WIDTH = 768 // iPad

let SYSTEM_VERSION = UIDevice.current.systemVersion

let COLOR_GRAY = UIColor(red: 229/255.0, green: 229/255.0, blue: 229/255.0, alpha: 1.0)
let COLOR_TEXT = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1.0)
let COLOR_WHITE = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)

let SCREEN_WIDTH  = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT  = UIScreen.main.bounds.size.height
let SCREEN_MAX_LENGTH  = (max(SCREEN_WIDTH, SCREEN_HEIGHT))
let SCREEN_MIN_LENGTH  = (min(SCREEN_WIDTH, SCREEN_HEIGHT))

let FONT_RAJDHANI_REGULAR = "Rajdhani-Regular"
let FONT_RAJDHANI_SEMIBOLD = "Rajdhani-SemiBold"
let FONT_RAJDHANI_BOLD = "Rajdhani-Bold"


let APP_DELEGATE  = UIApplication.shared.delegate as! AppDelegate
let storyBoardMain = UIStoryboard(name: "Main", bundle: Bundle.main)
let RESIGN_KEYBOARD = UIApplication.shared.keyWindow?.endEditing(true)

let X_ACCESS_TOKEN = "x-access-token"

