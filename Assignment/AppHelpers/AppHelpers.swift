//
//  AppHelpers.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import Foundation
import UIKit


/* Network Checking */

extension NSObject
{
    func checkNet() -> Bool
    {
        let reachability = Reachability()
        let status = reachability?.connection
        print("Connection status:",status! )
        switch status{
        case .wifi?:
            print("Reachable via WiFi")
            return true
        case .cellular?:
            print("Reachable via Cellular")
            return true
        case .none:
            print("Network not reachable")
            return false
        case .some(.none):
            print("Network not reachable")
            return false
        }
    }
}


func setShadowEffectForTopView(bgView: Any, cornerRadius: CGFloat)
{
    let view = bgView as! UIView
    view.layer.cornerRadius = cornerRadius
    view.layer.shadowColor = COLOR_GRAY.cgColor
    view.layer.shadowOpacity = 1.0
    view.layer.shadowOffset = CGSize(width: -2, height: 2)
    view.layer.shadowRadius = 0.3
    view.layer.masksToBounds = false
}

func setShadowEffectForView(bgView: UIView, cornerRadius: CGFloat)
{
    
    bgView.layer.cornerRadius = cornerRadius
    bgView.layer.shadowOffset = CGSize(width: -1, height: -1)
    bgView.layer.shadowRadius = 3.0
    bgView.layer.shadowOpacity = 0.2
    bgView.layer.masksToBounds = false
    bgView.layer.shadowColor = UIColor.black.cgColor
}


func setAttrStrForLabel(str1: String, str2: String ) -> NSAttributedString
{
    let str1Attributes: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font: setRandomFont(withSize: 13.0, withFontName: FONT_RAJDHANI_SEMIBOLD),
        NSAttributedString.Key.foregroundColor: UIColor.black]
    
    let str2Attributes: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font: setRandomFont(withSize: 13.0, withFontName: FONT_RAJDHANI_REGULAR),
        NSAttributedString.Key.foregroundColor: UIColor.black]
    
    let attString = NSMutableAttributedString()
    attString.append(NSAttributedString(string:str1, attributes: str1Attributes))
    attString.append(NSAttributedString(string:str2, attributes: str2Attributes))
    
    return attString
}


/* Loader Spinner Setting */

func showLoader(message : String) -> SampleView {
    
    let spinnerview = SpinnerCustomView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT))
    //spinnerview.layer.cornerRadius = 15.0f;
    // spinnerview.layer.masksToBounds = YES;
    spinnerview.titleLabel.text = message
    spinnerview.backgroundColor = UIColor.clear
    let spinner = SampleView.popup(withContentView: spinnerview)
    
    spinner?.backgroundColor = UIColor.clear
    spinner?.shouldDismissOnBackgroundTouch = false
    spinner?.show()
    
    return spinner!
}

func hideLoader(loader : SampleView) {
    loader.removeFromSuperview()
    loader.dismiss(true)
}



/* Font Setting */
func setRandomFont(withSize size : CGFloat, withFontName fontName: String) -> UIFont
{
    var font : UIFont!
    if SCREEN_HEIGHT ==  CGFloat(IPHONE_4_SCREEN_HEIGHT)
    {
        font = UIFont(name: fontName, size: size)!
    }
    else if (SCREEN_HEIGHT == CGFloat(IPHONE_5_SCREEN_HEIGHT)) // iPhone 5,5S, SE
    {
        font = UIFont(name: fontName, size: size+1)!
    }
    else if (SCREEN_WIDTH == CGFloat(IPHONE_6_SCREEN_WIDTH)) // iPhone 6, 6S, 7, Xs
    {
        font = UIFont(name: fontName, size: size+2)!
    }
    else if (SCREEN_WIDTH == CGFloat(IPHONE_6PLUS_SCREEN_WIDTH)) // iPhone 6 Plus, 6s Plus, 7 Plus, 8 Plus XR, Xs Max
    {
        font = UIFont(name: fontName, size: size+4)!
    }
    else if (SCREEN_WIDTH == CGFloat(768)) // iPad
    {
        font = UIFont(name: fontName, size: size+5)!
    }
    else
    {
        font = UIFont(name: fontName, size: size+4)!
    }
    return font
}


// MARK:- String Null check Validation

func nullConditionForString(str : Any) -> String {
    
    if let value = str as? String {
        if(str as? String == "null" || str as? String == "<null>"){
            return ""
        }
        return value
    }else{
        return ""
    }
}

// MARK:- Bool Null check Validation

func nullConditionForBool(boolValue : Any) -> Bool {
    if let value = boolValue as? Bool {
        return value
    }else{
        return false
    }
}

// MARK:- Array Null check Validation

func nullConditionForArray(array : Any) -> Array<Any> {
    if let value = array as? Array<Any> {
        return value
    }else{
        return []
    }
}

// MARK:- Int Null check Validation

func nullConditionForInt(intValue : Any) -> Int {
    if let value = intValue as? Int {
        return value
    }else{
        return 0
    }
}

// MARK:- Double Null check Validation

func nullConditionForDouble(doubleValue : Any) -> Double {
    if let value = doubleValue as? Double {
        return value
    }else{
        return 0
    }
}

// MARK:- Float Null check Validation

func nullConditionForFloat(floatValue : Any) -> Float {
    if let value = floatValue as? Float {
        return value
    }else{
        return 0
    }
}


// MARK:- Dictionary Null check Validation

func nullConditionForDictionary(dict : Any) -> Dictionary<String, Any> {
    if let value = dict as? Dictionary<String, Any> {
        return value
    }else{
        return [:]
    }
}

// MARK:- Data Null check Validation

func nullConditionForData(data : Any) -> Data {
    if let value = data as? Data {
        return value
    }else{
        return Data()
    }
}


// MARK:- UserDefaults Handle

func setUserDefaults(with value: Any, key:String) {
    UserDefaults.standard.set(value , forKey :key)
    UserDefaults.standard.synchronize()
}

func getUserDefaults(with key:String) -> Any {
    return UserDefaults.standard.value(forKey: key) ?? ""
}

