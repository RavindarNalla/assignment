//
//  JsonConverter.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class JsonConverter: NSObject {
    class var sharedInstance: JsonConverter
    {
        struct Static
        {
            static let instance: JsonConverter = JsonConverter()
        }
        return Static.instance
    }
}
