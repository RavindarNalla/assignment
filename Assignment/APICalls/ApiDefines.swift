//
//  ApiDefines.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import Foundation
/* Development */

let BASE_URL = "https://api.github.com/"

/* Production */
//let BASE_URL = ""

enum apiMethodName : Int {
    case getRepositories
    case getContributors
    case getRepolist
}
