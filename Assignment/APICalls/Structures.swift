//
//  Structures.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import Foundation

struct structGetRepositories {
    
    var strName = ""
    var strFull_name = ""

    var strWatchers_count = Int()
    var strCommit_count = ""
    
    var strAvatar_url = ""
    var strProjectLink = ""
    var strDescription = ""
    
    var strContributors_url = ""
    var intSize = Int()


    
}

struct structGetContributors {
    
    var strName = ""
    var strAvatar_url = ""
    
    var strRepo_url = ""
}

