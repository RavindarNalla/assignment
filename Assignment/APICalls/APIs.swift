//
//  APIs.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit
let SERVER_ERROR = 500

typealias ComplitionHandler = (_ response: Any, _ error: Error?, _ isSuccess: Bool) -> Void

class APIs: NSObject {
    // MARK: --- Shared Instatnce
    class var sharedInstance: APIs
    {
        struct  Static
        {
            static let instance : APIs = APIs()
        }
        return Static.instance
    }
    
    // MARK: --- Service_CallWithData
    func Service_Call(withParameters dicParameters : Any?,  withMethodName methodName:apiMethodName, complitionHandler : @escaping ComplitionHandler)
    {
        if(!checkNet())
        {
            complitionHandler("Please check your Internet connection.", nil, false);
            return;
        }
        
        let jsonData : Data? = convertRequest(withApiMethodName: methodName, withObject: dicParameters)
        let urlString : String
        if methodName == .getRepositories {
            urlString = "\(BASE_URL)\(getURLforMethod(methodName, AppendParams: dicParameters as Any))"
        }
        else{
            urlString = "\(getURLforMethod(methodName, AppendParams: dicParameters as Any))"
        }
        
        let url = URL.init(string: urlString)!
        
        print("URL ===>> : Url :\(urlString)")
        
        var  request = NSMutableURLRequest.init(url: url)
        if jsonData != nil
        {
            request.httpBody = jsonData
            let jsonString = String(data: jsonData!, encoding: .utf8)!
            request.addValue("\(String(describing: jsonString.count))", forHTTPHeaderField: "Content-Length")
            
            print("JSON Request ++++>>> : \(jsonString)")
        }
        request = getHeaders(methodName, with: request)
        request.timeoutInterval = 120
//        print("Request method type: \(request.httpMethod)")
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let sessionTask : URLSessionTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            let  httpResponse = response as? HTTPURLResponse
            
            if httpResponse == nil {
                complitionHandler("NO_RESPONSE", error, false)
                return
            }
            
            if httpResponse?.statusCode == 500
            {
                complitionHandler("Error occured while connecting to server.", error, false)
                return
            }
            if (error != nil) || response == nil
            {
                complitionHandler("Error occured while connecting to server.", error, false)
            }
            else
            {
                let jsonResponse = String(data: data!, encoding: .utf8)
//                print("Response : \(jsonResponse!)")
                
                if jsonResponse == "" {
                    complitionHandler("Invalid Credentials. Please try again...", error, false)
                }
                else  {
                    
        
                    let dicResponse : NSDictionary = nullConditionForDictionary(dict: (try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as Any) as NSDictionary

                    if dicResponse.count == 0 {
                        
                        let arrResponse : NSArray = nullConditionForArray(array: (try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as Any) as NSArray
                         complitionHandler(arrResponse, error, true)
                        
                    }else{
                        
                   
                         complitionHandler(dicResponse, error, true)
                    }
                    
                   
                    
                }
            }
            
            dispatchGroup.leave()
        }
        sessionTask.resume()
    }
    
    
    // MARK: --- Convert Request
    func convertRequest(withApiMethodName methodName:apiMethodName, withObject object:Any?) -> Data?
    {
      //  let objJsonConverter = JsonConverter.sharedInstance
        switch methodName
        {
        case .getRepositories:
            return nil
        case .getContributors:
            return nil
        case .getRepolist:
            return nil
        }
        
    }
    
    
    // MARK: --- get URL for Method
    func getURLforMethod(_ MethodName: apiMethodName, AppendParams: Any ) -> String
    {
        
        var url: String
        switch MethodName
        {
        case .getRepositories:
            url = updateUrlwithMethod(.getRepositories, Params: AppendParams)
            
        case .getContributors:
            url = updateUrlwithMethod(.getContributors, Params: AppendParams)
            
        case .getRepolist:
            url = updateUrlwithMethod(.getRepolist, Params: AppendParams)
            
        }
        
        return url
    }
    
    
    
    // MARK: --- update URL for Method
    
    func updateUrlwithMethod(_ MethodName: apiMethodName, Params:Any ) -> String
    {
        var url = ""
        switch (MethodName)
        {
        case .getRepositories:
            let searchText = Params as! String
            url = String(describing:"search/repositories?q=\(searchText)&order=desc&per_page=10")
            break
            
        case .getContributors:
            let contributorUrl = Params as! String
            url = String(describing:"\(contributorUrl)")
            break
            
        case .getRepolist:
            let repoListUrl = Params as! String
            url = String(describing:"\(repoListUrl)")
            break
            
        }
        return url
        
    }
    
    // MARK: --- Header methods
    func getHeaders(_ methodName: apiMethodName, with request: NSMutableURLRequest) -> NSMutableURLRequest
    {
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        switch methodName
        {
        case .getRepositories,
             .getContributors,
             .getRepolist:
            request.httpMethod = "GET"
            
        }
        return request
    }
    
    
    
}
