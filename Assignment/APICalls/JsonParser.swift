//
//  JsonParser.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class JsonParser: NSObject {
    
    class var sharedInstance: JsonParser
    {
        struct Static
        {
            static let instance: JsonParser = JsonParser()
        }
        return Static.instance
    }
    
    // Parsing Repositories items data
    func getRepositoriesDetails(resultObj:NSDictionary) -> NSMutableArray
    {
        let arrRepositories : NSMutableArray = []
        
        let dicResponse = nullConditionForDictionary(dict: resultObj)
        
        if dicResponse.count > 0 {
            
            
            let arrRepositoriesObj = nullConditionForArray(array: dicResponse["items"] as Any) as NSArray
            
            if arrRepositoriesObj.count > 0 {
                
                for  dic: Any in arrRepositoriesObj
                {
                    if dic is NSDictionary
                    {
                        let dicData = dic as! NSDictionary
                        arrRepositories .add(ParseRepositoriesDetails(dicData: dicData))
                    }
                }
            }
            else
            {
                let dicRepositoriesObj = nullConditionForDictionary(dict: dicResponse["items"] as Any) as NSDictionary
                
                if dicRepositoriesObj.count > 0 {
                    arrRepositories .add(ParseRepositoriesDetails(dicData: dicRepositoriesObj))
                }
                else {
                    print("no repositories found")
                }
                
            }
            
        }
        return arrRepositories
    }
    
    
    func ParseRepositoriesDetails(dicData: NSDictionary) -> structGetRepositories {
        
        var objStruct = structGetRepositories()
        
        objStruct.strName = nullConditionForString(str: dicData["name"] as Any)
        objStruct.strFull_name = nullConditionForString(str: dicData["full_name"] as Any)

        objStruct.strWatchers_count = nullConditionForInt(intValue: dicData["watchers_count"] as Any)
        
        objStruct.intSize = nullConditionForInt(intValue: dicData["size"] as Any)

//        objStruct.strCommit_count = nullConditionForString(str: dicData["watchers_count"] as Any)
//        if objStruct.strCommit_count == "" {
//            objStruct.strCommit_count = "\(nullConditionForInt(intValue: dicData["watchers_count"] as Any))"
//        }
        
        objStruct.strAvatar_url = (nullConditionForDictionary(dict: dicData["owner"] as Any) as NSDictionary).value(forKey: "avatar_url") as! String
        objStruct.strProjectLink = nullConditionForString(str: dicData["html_url"] as Any)
            //(nullConditionForDictionary(dict: dicData["owner"] as Any) as NSDictionary).value(forKey: "html_url") as! String
        objStruct.strDescription = nullConditionForString(str: dicData["description"] as Any)
        
        objStruct.strContributors_url = nullConditionForString(str: dicData["contributors_url"] as Any)

        return objStruct
        
    }
 
    
    // Parsing Contributors data
    func getContributorsDetails(resultObj:NSArray) -> NSMutableArray
    {
        let arrRepositories : NSMutableArray = []
        
        let arrResponse = nullConditionForArray(array: resultObj)
        
            if arrResponse.count > 0 {
                
                for  dic: Any in arrResponse
                {
                    if dic is NSDictionary
                    {
                        let dicData = dic as! NSDictionary
                        arrRepositories .add(ParseContributorsDetails(dicData: dicData))
                    }
                }
            }
        
        return arrRepositories
    }
    
    
    func ParseContributorsDetails(dicData: NSDictionary) -> structGetContributors {
        
        var objStruct = structGetContributors()
        
        objStruct.strName = nullConditionForString(str: dicData["login"] as Any)
        objStruct.strAvatar_url = nullConditionForString(str: dicData["avatar_url"] as Any)
        objStruct.strRepo_url = nullConditionForString(str: dicData["repos_url"] as Any)
        
        return objStruct
        
    }
    
    
    // Parsing Contributors data
    func getRepoListDetails(resultObj:NSArray) -> NSMutableArray
    {
        let arrRepositories : NSMutableArray = []
        
        let arrResponse = nullConditionForArray(array: resultObj)
        
        if arrResponse.count > 0 {
            
            for  dic: Any in arrResponse
            {
                if dic is NSDictionary
                {
                    let dicData = dic as! NSDictionary
                    arrRepositories .add(ParseRepositoriesDetails(dicData: dicData))
                }
            }
        }
        
        return arrRepositories
    }
}
