//
//  HomeTableViewCell.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var viewCellBg: UIView! {
        didSet {
            setShadowEffectForView(bgView: viewCellBg, cornerRadius: 8.0)
        }
    }
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_REGULAR)
        }
    }
    @IBOutlet weak var lblFullName: UILabel! {
        didSet {
            lblFullName.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_REGULAR)
        }
    }
    @IBOutlet weak var lblWatcherCount: UILabel! {
        didSet {
            lblWatcherCount.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_REGULAR)
        }
    }
    @IBOutlet weak var lblCommitCount: UILabel! {
        didSet {
            lblCommitCount.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_REGULAR)
        }
    }
    
    
    var structRepo : structGetRepositories? {
        didSet {
            // Update your subviews here.
            
            lblName.attributedText = setAttrStrForLabel(str1: "Name : ", str2: "\(String(describing: structRepo!.strName))")
            lblFullName.attributedText = setAttrStrForLabel(str1: "Full Name : ", str2: "\(String(describing: structRepo!.strFull_name))")
            lblWatcherCount.attributedText = setAttrStrForLabel(str1: "Watchers Count : ", str2: "\(String(describing: structRepo!.strWatchers_count))")
            lblCommitCount.attributedText = setAttrStrForLabel(str1: "Commit Count : ", str2: "\(String(describing: structRepo!.strCommit_count))")

            imgView.dowloadFromServer(link: structRepo!.strAvatar_url, contentMode: .scaleAspectFit)

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension UIImageView {
    func dowloadFromServer(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func dowloadFromServer(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        
        guard let url = URL(string: link) else { return }
        dowloadFromServer(url: url, contentMode: mode)
    }
}
