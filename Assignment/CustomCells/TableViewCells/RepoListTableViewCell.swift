//
//  RepoListTableViewCell.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class RepoListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = setRandomFont(withSize: 14.0, withFontName: FONT_RAJDHANI_REGULAR)
        }
    }
    
    var structRepo : structGetRepositories? {
        didSet {
            // Update your subviews here.
            lblName.text = structRepo?.strName
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
