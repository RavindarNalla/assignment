//
//  ContributorsCollectionViewCell.swift
//  Assignment
//
//  Created by Ravindar on 31/03/19.
//  Copyright © 2019 Mapprr. All rights reserved.
//

import UIKit

class ContributorsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBg: UIView!{
        didSet {
            setShadowEffectForView(bgView: viewCellBg, cornerRadius: 4.0)
        }
    }
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = setRandomFont(withSize: 8.0, withFontName: FONT_RAJDHANI_REGULAR)
        }
    }
    
    var structContributor : structGetContributors? {
        didSet {
            // Update your subviews here.
            lblName.text = structContributor?.strName
            
            imgView.dowloadFromServer(link: structContributor!.strAvatar_url, contentMode: .scaleAspectFit)
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
